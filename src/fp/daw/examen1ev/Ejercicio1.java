package fp.daw.examen1ev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Ejercicio1 {
	
	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
	    String entrada = null;
	    char[] caracteresArray = null;	    
	    
	    try {
    		System.out.print("Escribe cadena para ordenarlo: ");
			entrada = reader.readLine();
		    caracteresArray = entrada.toLowerCase().toCharArray();
			    
		    Arrays.sort(caracteresArray);
		    
		    System.out.println("Resultado del array char ordenado");
		    
		    System.out.println(String.valueOf(caracteresArray));
		    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
