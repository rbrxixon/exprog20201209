package fp.daw.examen1ev;

public class Ejercicio4 {
	
	static int [][] temporal = null;
	
	public static void imprimirMatriz(int [][] _temporal ) {
		int suma = 0;
		
		temporal = new int[3][3];
		for (int x = 0; x < _temporal[0].length; x++) {
			for (int y = 0; y < _temporal.length; y++) {
				System.out.print(_temporal[x][y] + " ");
				suma += _temporal[x][y]; 
			}
		System.out.println();
		}
		System.out.println("Total Suma Matriz: " + suma);
		System.out.println("----------");
	}
	
	public static void exportarMatriz (int [][] matriz, int indexX, int indexY, int iXFinal, int iYFinal, int indexTX, int indexTY) {
		int aux;
		int [][] temporal = new int[3][3];
		for (int x = indexX; x <= iXFinal; x++) {
			for (int y = indexY; y <= iYFinal; y++) {
				aux = matriz[x][y];
				temporal[indexTX][indexTY] = aux;
				indexTY++;
			}
			indexTY = 0;
			indexTX++;
		}
		
		imprimirMatriz(temporal);
	}
	
	
	
	
	
	public static int[][] max3x3sum (int [][] matriz ) {
		int indexX = 0;
		int indexY = 0;
		int iXFinal = 2;
		int iYFinal = 2;
		int indexTX = 0;
		int indexTY = 0;
		
		exportarMatriz (matriz,indexX, indexY, iXFinal, iYFinal, indexTX, indexTY);

		/*
		for (int x = 0; x < matriz[0].length; x++) {
			for (int y = 0; y < matriz.length; y++) {
				
			}
		}
		*/
		
		while (iXFinal < matriz[0].length-1) {
			
			//System.out.println("Aumento indexX " + indexX + " iXFinal "  + iXFinal );
			exportarMatriz (matriz,indexX, indexY, iXFinal, iYFinal, indexTX, indexTY);
			indexX = (indexX + 1);
			iXFinal = (iXFinal + 1);
		}
		
		
		System.out.println("iYfinal" +iYFinal );
		System.out.println("matriz.length-1 " + (matriz.length-1));
		while (iYFinal < matriz.length-1) {
			System.out.println("Aumento indexY " + indexY + " iYFinal "  + iYFinal );
			exportarMatriz (matriz,indexX, indexY, iXFinal, iYFinal, indexTX, indexTY);
			indexY = (indexY + 1);
			iYFinal = (iYFinal + 1);
		}
		
		
		
		if (matriz.length < 3 || matriz[0].length < 3 )
			return null;
		
		return matriz;
	}

	public static void main(String[] args) {
		int [][] pruebaMatriz = {  
			{1,5,5,2,4},
			{2,1,4,14,3},
			{3,7,11,2,8},
			{4,8,12,16,4}
		};
		
		max3x3sum(pruebaMatriz);
		
		}

}
